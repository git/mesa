/*
 * Copyright © 2016 Red Hat.
 * Copyright © 2016 Bas Nieuwenhuizen
 *
 * based in part on anv driver which is:
 * Copyright © 2015 Intel Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include "libresoc_shader.h"

static const struct nir_shader_compiler_options nir_options_llvm = {
	.vertex_id_zero_based = true,
	.lower_scmp = true,
	.lower_flrp16 = true,
	.lower_flrp32 = true,
	.lower_flrp64 = true,
	.lower_device_index_to_zero = true,
	.lower_fsat = true,
	.lower_fdiv = true,
	.lower_fmod = true,
	.lower_bitfield_insert_to_bitfield_select = true,
	.lower_bitfield_extract = true,
	.lower_sub = true,
	.lower_pack_snorm_2x16 = true,
	.lower_pack_snorm_4x8 = true,
	.lower_pack_unorm_2x16 = true,
	.lower_pack_unorm_4x8 = true,
	.lower_unpack_snorm_2x16 = true,
	.lower_unpack_snorm_4x8 = true,
	.lower_unpack_unorm_2x16 = true,
	.lower_unpack_unorm_4x8 = true,
	.lower_extract_byte = true,
	.lower_extract_word = true,
	.lower_ffma = true,
	.lower_fpow = true,
	.lower_mul_2x32_64 = true,
	.lower_rotate = true,
	.use_scoped_barrier = true,
	.max_unroll_iterations = 32,
	.use_interpolated_input_intrinsics = true,
	/* nir_lower_int64() isn't actually called for the LLVM backend, but
	 * this helps the loop unrolling heuristics. */
	.lower_int64_options = nir_lower_imul64 |
                               nir_lower_imul_high64 |
                               nir_lower_imul_2x32_64 |
                               nir_lower_divmod64 |
                               nir_lower_minmax64 |
                               nir_lower_iabs64,
	.lower_doubles_options = nir_lower_drcp |
				 nir_lower_dsqrt |
				 nir_lower_drsq |
				 nir_lower_ddiv,
};

static char *
libresoc_dump_nir_shaders(struct nir_shader * const *shaders,
                      int shader_count)
{
	char *data = NULL;
	char *ret = NULL;
	size_t size = 0;
	FILE *f = open_memstream(&data, &size);
	if (f) {
		for (int i = 0; i < shader_count; ++i)
			nir_print_shader(shaders[i], f);
		fclose(f);
	}

	ret = malloc(size + 1);
	if (ret) {
		memcpy(ret, data, size);
		ret[size] = 0;
	}
	free(data);
	return ret;
}

nir_shader *
libresoc_shader_compile_to_nir(struct libresoc_device *device,
			   struct libresoc_shader_module *module,
			   const char *entrypoint_name,
			   gl_shader_stage stage,
			   const VkSpecializationInfo *spec_info,
			   const VkPipelineCreateFlags flags,
			   unsigned subgroup_size, unsigned ballot_bit_size)
{
	nir_shader *nir;
	const nir_shader_compiler_options *nir_options =
		&nir_options_llvm; 

	if (module->nir) {
		/* Some things such as our meta clear/blit code will give us a NIR
		 * shader directly.  In that case, we just ignore the SPIR-V entirely
		 * and just use the NIR shader */
		nir = module->nir;
		nir->options = nir_options;
		nir_validate_shader(nir, "in internal shader");

		assert(exec_list_length(&nir->functions) == 1);
	} else {
		uint32_t *spirv = (uint32_t *) module->data;
		assert(module->size % 4 == 0);

		if (device->instance->debug_flags & LIBRESOC_DEBUG_DUMP_SPIRV)
			libresoc_print_spirv(module->data, module->size, stderr);

		uint32_t num_spec_entries = 0;
		struct nir_spirv_specialization *spec_entries = NULL;
		if (spec_info && spec_info->mapEntryCount > 0) {
			num_spec_entries = spec_info->mapEntryCount;
			spec_entries = calloc(num_spec_entries, sizeof(*spec_entries));
			for (uint32_t i = 0; i < num_spec_entries; i++) {
				VkSpecializationMapEntry entry = spec_info->pMapEntries[i];
				const void *data = spec_info->pData + entry.offset;
				assert(data + entry.size <= spec_info->pData + spec_info->dataSize);

				spec_entries[i].id = spec_info->pMapEntries[i].constantID;
				switch (entry.size) {
				case 8:
					spec_entries[i].value.u64 = *(const uint64_t *)data;
					break;
				case 4:
					spec_entries[i].value.u32 = *(const uint32_t *)data;
					break;
				case 2:
					spec_entries[i].value.u16 = *(const uint16_t *)data;
					break;
				case 1:
					spec_entries[i].value.u8 = *(const uint8_t *)data;
					break;
				default:
					assert(!"Invalid spec constant size");
					break;
				}
			}
		}

		const struct spirv_to_nir_options spirv_options = {0};
		nir = spirv_to_nir(spirv, module->size / 4,
				   spec_entries, num_spec_entries,
				   stage, entrypoint_name,
				   &spirv_options, nir_options);
		assert(nir->info.stage == stage);
		nir_validate_shader(nir, "after spirv_to_nir");

		if (device->instance->debug_flags & LIBRESOC_DEBUG_DUMP_NIR)
			nir_print_shader(nir, stderr);
		free(spec_entries);
	}
return nir;
}

VkResult
libresoc_CreateShaderModule(VkDevice _device,
		const VkShaderModuleCreateInfo *pCreateInfo,
		const VkAllocationCallbacks *pAllocator,
		VkShaderModule *pShaderModule)
{
	LIBRESOC_FROM_HANDLE(libresoc_device, device, _device);
	struct libresoc_shader_module *module;

	assert(pCreateInfo->sType == VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO);
	assert(pCreateInfo->flags == 0);

	module = vk_alloc2(&device->vk.alloc, pAllocator,
			     sizeof(*module) + pCreateInfo->codeSize, 8,
			     VK_SYSTEM_ALLOCATION_SCOPE_OBJECT);
	if (module == NULL)
		return vk_error(device->instance, VK_ERROR_OUT_OF_HOST_MEMORY);

	vk_object_base_init(&device->vk, &module->base,
			    VK_OBJECT_TYPE_SHADER_MODULE);

	module->nir = NULL;
	module->size = pCreateInfo->codeSize;
	memcpy(module->data, pCreateInfo->pCode, module->size);

	_mesa_sha1_compute(module->data, module->size, module->sha1);

	*pShaderModule = libresoc_shader_module_to_handle(module);

	return VK_SUCCESS;
}

void
libresoc_DestroyShaderModule(VkDevice _device,
		VkShaderModule _module,
		const VkAllocationCallbacks *pAllocator)
{
	LIBRESOC_FROM_HANDLE(libresoc_device, device, _device);
	LIBRESOC_FROM_HANDLE(libresoc_shader_module, module, _module);

	if (!module)
		return;

	vk_object_base_finish(&module->base);
	vk_free2(&device->vk.alloc, pAllocator, module);
}
