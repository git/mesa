/*
 * Copyright © 2016 Red Hat.
 * Copyright © 2016 Bas Nieuwenhuizen
 *
 * based in part on anv driver which is:
 * Copyright © 2015 Intel Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include "libresoc_private.h"
#include "libresoc_shader.h"
#include "vk_util.h"

#include "vk_format.h"
#include "util/debug.h"

VkResult libresoc_create_shaders(struct libresoc_pipeline *pipeline,
                             struct libresoc_device *device,
                             const VkPipelineShaderStageCreateInfo **pStages,
                             const VkPipelineCreateFlags flags) 
{
	struct libresoc_shader_module *modules[MESA_SHADER_STAGES] = { 0, };
	nir_shader *nir[MESA_SHADER_STAGES] = {0};

	for (unsigned i = 0; i < MESA_SHADER_STAGES; ++i) {
		if (pStages[i]) {
			modules[i] = libresoc_shader_module_from_handle(pStages[i]->module);
			pipeline->active_stages |= mesa_to_vk_shader_stage(i);
		}
	}


	for (unsigned i = 0; i < MESA_SHADER_STAGES; ++i) {
		const VkPipelineShaderStageCreateInfo *stage = pStages[i];
		unsigned subgroup_size = 64, ballot_bit_size = 64;

		if (!modules[i])
			continue;

		nir[i] = libresoc_shader_compile_to_nir(device, modules[i],
						    stage ? stage->pName : "main", i,
						    stage ? stage->pSpecializationInfo : NULL,
						    flags, 
						    subgroup_size, ballot_bit_size);

		/* We don't want to alter meta shaders IR directly so clone it
		 * first.
		 */
		if (nir[i]->info.name) {
			nir[i] = nir_shader_clone(NULL, nir[i]);
		}

	}


	return VK_SUCCESS;
}
static VkResult
libresoc_pipeline_init(struct libresoc_pipeline *pipeline,
		   struct libresoc_device *device,
		   const VkGraphicsPipelineCreateInfo *pCreateInfo)
{
	VkResult result;
	pipeline->device = device;

	const VkPipelineShaderStageCreateInfo *pStages[MESA_SHADER_STAGES] = { 0, };
	for (uint32_t i = 0; i < pCreateInfo->stageCount; i++) {
		gl_shader_stage stage = ffs(pCreateInfo->pStages[i].stage) - 1;
		pStages[stage] = &pCreateInfo->pStages[i];
	}

	result = libresoc_create_shaders(pipeline, device, pStages,
		                     pCreateInfo->flags);
	if (result != VK_SUCCESS)
		return result;
	
	//TODO: add more code as required
	return result;
}

VkResult
libresoc_graphics_pipeline_create(
	VkDevice _device,
	VkPipelineCache _cache,
	const VkGraphicsPipelineCreateInfo *pCreateInfo,
	const VkAllocationCallbacks *pAllocator,
	VkPipeline *pPipeline)
{
	LIBRESOC_FROM_HANDLE(libresoc_device, device, _device);
	struct libresoc_pipeline *pipeline;
	VkResult result;

	pipeline = vk_zalloc2(&device->vk.alloc, pAllocator, sizeof(*pipeline), 8,
			      VK_SYSTEM_ALLOCATION_SCOPE_OBJECT);
	if (pipeline == NULL)
		return vk_error(device->instance, VK_ERROR_OUT_OF_HOST_MEMORY);

	vk_object_base_init(&device->vk, &pipeline->base,
			    VK_OBJECT_TYPE_PIPELINE);

	result = libresoc_pipeline_init(pipeline, device,
				    pCreateInfo);
	if (result != VK_SUCCESS) {
		//libresoc_pipeline_destroy(device, pipeline, pAllocator);
		return result;
	}

	*pPipeline = libresoc_pipeline_to_handle(pipeline);

	return VK_SUCCESS;
}

VkResult libresoc_CreateGraphicsPipelines(
	VkDevice                                    _device,
	VkPipelineCache                             pipelineCache,
	uint32_t                                    count,
	const VkGraphicsPipelineCreateInfo*         pCreateInfos,
	const VkAllocationCallbacks*                pAllocator,
	VkPipeline*                                 pPipelines)
{
	VkResult result = VK_SUCCESS;
	unsigned i = 0;

	for (; i < count; i++) {
		VkResult r;
		r = libresoc_graphics_pipeline_create(_device,
						  pipelineCache,
						  &pCreateInfos[i],
						  pAllocator, &pPipelines[i]);
		if (r != VK_SUCCESS) {
			result = r;
			pPipelines[i] = VK_NULL_HANDLE;

			if (pCreateInfos[i].flags & VK_PIPELINE_CREATE_EARLY_RETURN_ON_FAILURE_BIT_EXT)
				break;
		}
	}

	for (; i < count; ++i)
		pPipelines[i] = VK_NULL_HANDLE;

	return result;
}

VkResult libresoc_CreateComputePipelines(
	VkDevice                                    _device,
	VkPipelineCache                             pipelineCache,
	uint32_t                                    count,
	const VkComputePipelineCreateInfo*          pCreateInfos,
	const VkAllocationCallbacks*                pAllocator,
	VkPipeline*                                 pPipelines)
{
	VkResult result = VK_SUCCESS;
        //FIXME: stub

	return result;
}

void libresoc_DestroyPipeline(
	VkDevice                                    _device,
	VkPipeline                                  _pipeline,
	const VkAllocationCallbacks*                pAllocator)
{
        //FIXME: stub
}
