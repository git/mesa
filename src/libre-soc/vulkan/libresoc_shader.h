/*
 * Copyright © 2016 Red Hat.
 * Copyright © 2016 Bas Nieuwenhuizen
 *
 * based in part on anv driver which is:
 * Copyright © 2015 Intel Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#ifndef LIBRESOC_SHADER_H
#define LIBRESOC_SHADER_H

#include "libresoc_constants.h"

#include "util/mesa-sha1.h"
#include "nir/nir.h"
#include "nir/nir_builder.h"
#include "spirv/nir_spirv.h"
#include "libresoc_private.h"
#include "nir/nir.h"
#include "vulkan/vulkan.h"
#include "vulkan/util/vk_object.h"

struct libresoc_device;

struct libresoc_shader_module {
	struct vk_object_base base;
	struct nir_shader *nir;
	unsigned char sha1[20];
	uint32_t size;
	char data[0];
};

nir_shader *
libresoc_shader_compile_to_nir(struct libresoc_device *device,
			   struct libresoc_shader_module *module,
			   const char *entrypoint_name,
			   gl_shader_stage stage,
			   const VkSpecializationInfo *spec_info,
			   const VkPipelineCreateFlags flags,
			   unsigned subgroup_size, unsigned ballot_bit_size);

void
libresoc_destroy_shader_slabs(struct libresoc_device *device);

VkResult
libresoc_create_shaders(struct libresoc_pipeline *pipeline,
		    struct libresoc_device *device,
		    const VkPipelineShaderStageCreateInfo **pStages,
		    const VkPipelineCreateFlags flags);

#endif
